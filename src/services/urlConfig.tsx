import axios, { AxiosInstance } from "axios";
import { createBrowserHistory } from "@remix-run/router";

const BASE_URL: string = "https://shop.cyberlearn.vn";
export const history = createBrowserHistory({ window });
export const https: AxiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
});
// intercepter
//response
https.interceptors.response.use(
  (response) => {
    // Any status code that lie within the range of 2xx cause this  to trigger
    // Do something with response data
    return response;
  },
  (error) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.status === 400 || error.status === 404) {
      history.push("/");
    }
    return Promise.reject(error);
  }
);
