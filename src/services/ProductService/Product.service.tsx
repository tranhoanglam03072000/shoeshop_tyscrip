import { https } from "../urlConfig";

export const productService = {
  getAllProduct: () => {
    let uri: string = "/api/Product";
    return https.get(uri);
  },
  getDetailProduct: (id: string) => {
    let uri: string = `/api/Product/getbyid?id=${id}`;
    return https.get(uri);
  },
};
