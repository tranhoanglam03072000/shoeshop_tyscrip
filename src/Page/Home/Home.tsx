import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard/ProductCard";
import { DispatchType, RootState } from "../../redux/configStore";
import {
  getProductApi,
  ProductModel,
} from "../../redux/ProductReducer/productReducer";

type Props = {};

export default function Home({}: Props) {
  const { arrProduct } = useSelector(
    (state: RootState) => state.productReducer
  );
  const dispatch: DispatchType = useDispatch();
  const getAllProductApi = () => {
    dispatch(getProductApi());
  };
  useEffect(() => {
    // call API
    getAllProductApi();
  }, []);
  return (
    <div className="container mx-auto">
      <h3>Product feature</h3>
      <div className="grid grid-cols-4 gap-12">
        {arrProduct.map((prod: ProductModel, index: number) => {
          return (
            <div key={prod.id}>
              <ProductCard prod={prod} />
            </div>
          );
        })}
      </div>
    </div>
  );
}
