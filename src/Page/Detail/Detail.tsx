import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard/ProductCard";
import {
  getProductDetailApi,
  RelatedProduct,
} from "../../redux/ProductReducer/productReducer";
import { DispatchType, RootState } from "../../redux/configStore";
import { Params, useParams } from "react-router-dom";

type Props = {};

export default function Detail({}: Props) {
  const { productDetail } = useSelector(
    (state: RootState) => state.productReducer
  );
  const dispatch: DispatchType = useDispatch();
  const params: Readonly<Params<string>> = useParams();
  const getProductByIdApi = () => {
    // lấy id từ url
    const id: string | undefined = params.id;
    // dispatch thunk
    const actionThunk = getProductDetailApi(id as string);
    dispatch(actionThunk);
  };
  useEffect(() => {
    getProductByIdApi();
  }, [params.id]);
  return (
    <div className="">
      <div className="flex">
        <div className="w-1/3">
          <img
            src={productDetail?.image}
            alt=""
            className="w-full h-56 object-cover"
          />
        </div>
        <div className="w-2/3">
          <h3>{productDetail?.name}</h3>
          <p>{productDetail?.description}</p>
        </div>
      </div>
      <h3 className="text-center text-2xl font-bold">Relate Product</h3>
      <div className="grid grid-cols-3 gap-24">
        {productDetail?.relatedProducts.map(
          (prod: RelatedProduct, index: number) => {
            return <ProductCard prod={prod} />;
          }
        )}
      </div>
    </div>
  );
}
