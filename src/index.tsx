import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import {
  unstable_HistoryRouter as HistoryBrowser,
  Route,
  Routes,
} from "react-router-dom";
import HomeTeamplate from "./templates/HomeTeamplate";
import { Routers } from "./Routers/Router";
import "./assets/scss/style.scss";
import { Provider } from "react-redux";
import { store } from "./redux/configStore";
import Home from "./Page/Home/Home";
import { history } from "./services/urlConfig";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
    <HistoryBrowser history={history}>
      <Routes>
        <Route path="" element={<HomeTeamplate />}>
          <Route index element={<Home />} />
          {Routers.map((route, index: number) => {
            return (
              <Route key={index} path={route.path} element={route.element} />
            );
          })}
        </Route>
      </Routes>
    </HistoryBrowser>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
