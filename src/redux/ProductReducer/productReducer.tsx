import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { productService } from "../../services/ProductService/Product.service";
import { DispatchType } from "../configStore";

export interface ProductDetailModel {
  id: number;
  name: string;
  alias: string;
  price: number;
  feature: boolean;
  description: string;
  size: string[];
  shortDescription: string;
  quantity: number;
  image: string;
  categories: Category[];
  relatedProducts: RelatedProduct[];
}

export interface Category {
  id: string;
  category: string;
}

export interface RelatedProduct {
  id: number;
  name: string;
  alias: string;
  feature: boolean;
  price: number;
  description: string;
  shortDescription: string;
  image: string;
}
export interface ProductModel {
  id: number;
  name: string;
  alias: string;
  price: number;
  description: string;
  size: string;
  shortDescription: string;
  quantity: number;
  deleted: boolean;
  categories: string;
  relatedProducts: string;
  feature: boolean;
  image: string;
}

export type ProductState = {
  arrProduct: ProductModel[];
  productDetail: ProductDetailModel | null;
};

const initialState: ProductState = {
  arrProduct: [
    {
      id: 1,
      name: "abc",
      alias: "alias",
      price: 123,
      description: "mô tả",
      size: "22px",
      shortDescription: "mô tả ngắn",
      quantity: 20,
      deleted: false,
      categories: "tại sao ",
      relatedProducts: "là sao",
      feature: true,
      image: "htppmmmm",
    },
  ],
  productDetail: null,
};

const productReducer = createSlice({
  name: "productReducer",
  initialState,
  reducers: {
    setArrayProductAction: (
      state: ProductState,
      action: PayloadAction<ProductModel[]>
    ) => {
      state.arrProduct = action.payload;
    },
    //Extrareducer
  },
  extraReducers(builder) {
    // call Api  :
    builder.addCase(getProductDetailApi.pending, (state, action) => {
      // bật loading
    });
    builder.addCase(
      getProductDetailApi.fulfilled,
      (state: ProductState, action: PayloadAction<ProductDetailModel>) => {
        // tắt loading
        state.productDetail = action.payload;
      }
    );
    builder.addCase(getProductDetailApi.rejected, (state, action) => {});
  },
});

export const { setArrayProductAction } = productReducer.actions;

export default productReducer.reducer;

// ================================ Action api====

export const getProductApi = () => {
  return async (dispatch: DispatchType) => {
    try {
      const result = await productService.getAllProduct();
      const content: ProductModel[] = result.data.content;
      const action: PayloadAction<ProductModel[]> =
        setArrayProductAction(content);
      dispatch(action);
    } catch (err) {
      console.log(err);
    }
  };
};

// Cách 2: create async thunk
export const getProductDetailApi = createAsyncThunk(
  "productReducer/getProductDetailApi",
  async (id: string) => {
    const response = await productService.getDetailProduct(id);
    return response.data.content;
  }
);
