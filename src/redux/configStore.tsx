import { configureStore } from "@reduxjs/toolkit";
import { type } from "@testing-library/user-event/dist/type";
import productReducer from "./ProductReducer/productReducer";

export const store = configureStore({
  reducer: {
    productReducer,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export type DispatchType = typeof store.dispatch;
