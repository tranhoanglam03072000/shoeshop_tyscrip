import { Navigate } from "react-router-dom";
import Carts from "../Page/Carts/Carts";
import Detail from "../Page/Detail/Detail";
import Login from "../Page/Login/Login";
import Profile from "../Page/Profile/Profile";
import Register from "../Page/Register/Register";
import Search from "../Page/Search/Search";

export const Routers = [
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register />,
  },
  {
    path: "/carts",
    element: <Carts />,
  },
  {
    path: "/detail/:id",
    element: <Detail />,
  },
  {
    path: "/profile",
    element: <Profile />,
  },
  {
    path: "/search",
    element: <Search />,
  },
  {
    path: "*",
    element: <Navigate to="" />,
  },
];
