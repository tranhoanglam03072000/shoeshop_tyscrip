import React from "react";
import { NavLink } from "react-router-dom";
import {
  ProductModel,
  RelatedProduct,
} from "../../redux/ProductReducer/productReducer";

type Props = {
  prod?: ProductModel | RelatedProduct;
};

export default function ProductCard({ prod }: Props) {
  return (
    <div>
      <img
        className="rounded w-80 h-80"
        src={prod?.image ? prod.image : "https://i.pravatar.cc?u=2"}
        alt=""
      />
      <div className="card-body mb-4">
        <p className="text-xl font-bold">
          {prod?.name ? prod.name : "productname"}
        </p>
        <p>{prod?.shortDescription}</p>
      </div>
      <div className="card-footer flex  space-x-5 items-center">
        <NavLink
          className={
            "px-2 py-2 bg-green-500 hover:bg-green-600 text-white hover:text-white rounded w-1/2 text-center"
          }
          to={`/detail/${prod?.id}`}
        >
          {" "}
          Buy Now
        </NavLink>
        <div className="text-center w-1/2 font-medium text-blue-500">
          {prod?.price ? prod.price : "Pricde"} $
        </div>
      </div>
    </div>
  );
}
