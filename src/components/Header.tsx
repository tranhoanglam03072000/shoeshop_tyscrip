import React from "react";
import { NavLink } from "react-router-dom";

type Props = {};

export default function Header({}: Props) {
  return (
    <div className="header">
      <section className="logo-header">
        <div className="logo">
          <NavLink to="" className={"logo-link"}>
            <img src="./img/logo.png" alt="logo" />
          </NavLink>
        </div>
        <div className="nav-bar-seacrh">
          <div className="search">
            <NavLink className="search-link" to={"/search"}>
              TimKiem
            </NavLink>
          </div>
          <div className="carts">
            <NavLink className="carts-link" to={"/carts"}>
              GioHang
            </NavLink>
          </div>
          <div className="login">
            <NavLink className="login-link" to={"/login"}>
              DangNhap
            </NavLink>
          </div>
          <div className="register">
            <NavLink className="register-link" to={"/register"}>
              DangKy
            </NavLink>
          </div>
        </div>
      </section>
      <section className="menu flex items-center">
        <nav className="nav-menu space-x-2">
          <NavLink to="">Home</NavLink>
          <NavLink to="">Men</NavLink>
          <NavLink to="">Woman</NavLink>
          <NavLink to="">Sport</NavLink>
        </nav>
      </section>
    </div>
  );
}
